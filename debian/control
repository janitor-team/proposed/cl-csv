Source: cl-csv
Section: lisp
Priority: optional
Maintainer: Debian Common Lisp Team <debian-common-lisp@lists.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.0
Homepage: https://github.com/AccelerationNet/cl-csv
Vcs-Git: https://salsa.debian.org/common-lisp-team/cl-csv.git
Vcs-Browser: https://salsa.debian.org/common-lisp-team/cl-csv
Rules-Requires-Root: no

Package: cl-csv
Architecture: all
Depends: ${misc:Depends}, cl-iterate, cl-alexandria, cl-interpol
Conflicts: cl-csv-clsql, cl-csv-data-table
Replaces: cl-csv-clsql, cl-csv-data-table
Description: Common Lisp library providing easy CSV reading and writing
 This Common Lisp library aims to simplify working with CSVs:
 .
  * reads/writes CSVs from/to strings, streams and files
  * support streaming reads (allowing processing very large CSVs, through
    read-csv's row-fn parameter)
  * supports custom data formatting
  * settable quote, separator and quote-escapes
  * supports multiline quoted data
  * a test suite
  * detailed state about the process on error (line number, column number,
    char index), current collection state
